var cartModel  =  require('../models/cart.model');
var productModel =  require('../models/products.model');
var errHandler = require('../../helper/error_handler');

/**
 *
 * @param {*} req
 * @param {*} res
 *
 * Create a cart
 */

 module.exports.createCart = async (req, res) => {
    try {
  
    var cart = req.body;
    var cartIdFix = req.body.cart_id;
    var product_id = req.body.product_id;
    const quantity = req.body.quantity;
    const price = parseFloat(req.body.price);
    var productData = await productModel.findOne({where:{product_id: product_id}});
    if(productData){
        var cartExists  = await cartModel.findAll({where:{cart_id : req.body.cart_id}});
        if(cartExists.length != 0){
            const index = cartExists.findIndex(item => item.product_id == product_id);
            if (index !== -1) {
                cartExists[index].quantity = cartExists[index].quantity + quantity;
                cartExists[index].total = cartExists[index].quantity * price;
                cartExists[index].price = price;            
           var quantityUp = cartExists[index].quantity;
           var totalUp= cartExists[index].total;
            let cartData = await cartModel.update({quantity:quantityUp,total:totalUp},{where:{product_id: product_id}});
            if(cartData){
                res.status(201).json({success: true, valid :true, message: "Item has been added successfully.", cart_id: cartIdFix});
            }else{
                res.status(400).json({success: false, valid: false, message: "Item has not been added."});
            }
        }else{
            var cartData = await cartModel.create(cart); 
            if(cartData != ''){
                res.status(201).json({success: true, valid :true, message: "Item has been added successfully.", cart_id: cartData.cart_id});
            }else{
                res.status(400).json({success: false, valid: false, message: "Item has not been added."});
            }
        }
            
        }else{
           req.body.cart_id =   Math.floor(Math.random() * 123456789);
            var cartData = await cartModel.create(cart);
            if(cartData != ''){
                res.status(201).json({success: true, valid :true, message: "Item has been added successfully.", cart_id: cartData.cart_id});
            }else{
                res.status(400).json({success: false, valid: false, message: "Item has not been added."});
            }
        }
  
    }else{
        res.status(404).json({success: false, valid: false, message: "Product not found."});  
    }
  
    } catch (e) {
        await errHandler.errorHandler(res,e);

    }
}

/**
 *
 * @param {*} req
 * @param {*} res
 *
 * Get the data of a cart
 */

 module.exports.getCart = async (req, res) => {
    try {
        let cartData;
        var cart_id = req.params.cart_id;   
        cartData = await cartModel.findAll({
            where: {
                cart_id: cart_id
            },
            include: [
                {
                    model: productModel,
                    attributes:['product_id','name','description','sku','price','image']
                }
            ]

        });

        if (cartData) {
            res.status(200).json({
                success: true,
                valid: true,
                message: "Cart found.",
                cart: cartData
            });
        } else {
            res.status(404).json({
                success: false,
                valid: false,
                message: "Cart not found."
            });
        }
  
    } catch (e) {
        await errHandler.errorHandler(res,e);

    }
}

/**
 *
 * @param {*} req
 * @param {*} res
 *
 * Remove the item of a cart
 */

 module.exports.removeCartItem = async (req, res) => {
    try {
        let cartData;
        var cart_id = req.params.cart_id;
        var product_id = req.params.product_id   
        cartData = await cartModel.destroy({
            where: {
                cart_id: cart_id,
                product_id: product_id
            }

        });

        if (cartData) {
            res.status(200).json({
                success: true,
                valid: true,
                message: "Cart Item Removed.",
                cart: cartData
            });
        } else {
            res.status(404).json({
                success: false,
                valid: false,
                message: "Cart Item not found."
            });
        }
  
    } catch (e) {
        await errHandler.errorHandler(res,e);

    }
}