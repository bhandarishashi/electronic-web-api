var productModel = require('../models/products.model');
var errHandler = require('../../helper/error_handler');
var imageModel = require('../models/image.model');

/**
 *
 * @param {*} req
 * @param {*} res
 *
 * Get list of  all products
 */

module.exports.getProducts = async (req, res) => {
    try {

        let products, featured_products;
        var page = parseInt(req.query.page) - 1 || 0;
        var pageSize = parseInt(req.query.limit) || 2;
        var offset = page * pageSize;
        var limit = offset + pageSize;

        products = await productModel.findAndCountAll({
            limit: pageSize,
            offset: offset
        });
        featured_products = await productModel.findAndCountAll({
            where: {
                is_featured: true
            }
        })
        if (products.length == 0) {
            res.status(404).json({
                success: false,
                valid: false,
                message: "Products not found."
            });
        } else {
            res.status(200).json({
                success: true,
                valid: true,
                message: "Products found.",
                products: products,
                featured_products: featured_products
            });
        }

    } catch (e) {
        await errHandler.errorHandler(res, e);
    }
}


/**
 *
 * @param {*} req
 * @param {*} res
 *
 * Get detail of specific product
 */

module.exports.getProductDetail = async (req, res) => {
    try {
        let product;
        var pid = req.params.id;
        product = await productModel.findOne({
            where: {
                product_id: pid
            },
            include: [{
                model: imageModel,
                where: {
                    product_id: pid
                },
                required: false,
            }]
        });
        if (product) {
            res.status(200).json({
                success: true,
                valid: true,
                message: "Product found.",
                product: product
            });
        } else {
            res.status(404).json({
                success: false,
                valid: false,
                message: "Product not found."
            });
        }

    } catch (e) {
        await errHandler.errorHandler(res, e);


    }
}