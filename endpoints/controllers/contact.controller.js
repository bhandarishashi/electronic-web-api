var contactModel = require('../models/contact.model');
var errHandler = require('../../helper/error_handler');



/**
 *
 * @param {*} req
 * @param {*} res
 *
 * Post the inquiry
 */

 module.exports.postInquiry = async (req, res) => {
    try {
    var inquiryData = req.body;
    var inquiry = await contactModel.create(inquiryData);
    if(inquiry != ''){
        res.status(201).json({success: true, valid :true, message: "Message has been submitted successfully."});
    }else{
        res.status(400).json({success: false, valid: false, message: "Message has not been submitted/"});
    }
    } catch (e) {
        await errHandler.errorHandler(res,e);

    }
}