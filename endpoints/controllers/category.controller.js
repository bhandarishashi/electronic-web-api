var productModel = require('../models/products.model');
var errHandler = require('../../helper/error_handler');
const { Op } = require("sequelize");

/**
 *
 * @param {*} req
 * @param {*} res
 *
 * Get list of  all categories
 */

module.exports.getCategories = async (req, res) => {
    try {
        let categories;
        categories = await category.findAll();
        if (categories.length == 0) {
            res.status(404).json({
                success: false,
                valid: false,
                message: "Categories not found."
            });
        } else {
            res.status(200).json({
                success: true,
                valid: true,
                message: "Categories found.",
                categories: categories
            });
        }

    } catch (e) {
        await errHandler.errorHandler(res, e);

    }
}


/**
 *
 * @param {*} req
 * @param {*} res
 *
 * Get list of  all products by category
 */

module.exports.getProductsByCategory = async (req, res) => {
    try {
        let products,categoryData;
        let category_key = req.params.category_key;
        var page = parseInt(req.query.page) - 1 || 0;
        var pageSize = parseInt(req.query.limit) || 2;
        var offset = page * pageSize;
        var limit = offset + pageSize;
        products = await productModel.findAndCountAll({
            where: {
                category_id: category_key
            },
            limit: pageSize,
            offset: offset
        });
        categoryData = await category.findOne({where: {category_id: category_key}});
        if (products.rows.length == 0) {
            res.status(404).json({
                success: false,
                valid: false,
                message: "Products not found."
            });
        } else {
        const totalPages = Math.ceil(products.count / pageSize);

            res.status(200).json({
                success: true,
                valid: true,
                message: "Products found.",
                products: products,
                category: categoryData,
                total_page: totalPages
            });
        }

    } catch (e) {
        await errHandler.errorHandler(res, e);

    }
}

/**
 *
 * @param {*} req
 * @param {*} res
 *
 * Get list of  recommended products by category
 */

module.exports.getRecommendProductsByCategory = async (req, res) => {
    try {
        let products,categoryData;
        let category_key = req.params.category_key;
        let product_id = req.query.product_id;
        var page = parseInt(req.query.page) - 1 || 0;
        var pageSize = parseInt(req.query.limit) || 2;
        var offset = page * pageSize;
        var limit = offset + pageSize;
        products = await productModel.findAll({
            where: {
                category_id: category_key,
                product_id:  {  [Op.ne]: product_id }
            },
            limit: pageSize,
            offset: offset
        });
        categoryData = await category.findOne({where: {category_id: category_key}});
        if (products.length == 0) {
            res.status(404).json({
                success: false,
                valid: false,
                message: "Products not found."
            });
        } else {
        const totalPages = Math.ceil(products.count / pageSize);

            res.status(200).json({
                success: true,
                valid: true,
                message: "Products found.",
                products: products,
                category: categoryData,
                total_page: totalPages
            });
        }

    } catch (e) {
        await errHandler.errorHandler(res, e);

    }
}