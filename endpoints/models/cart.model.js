let Sequelize = require('sequelize');
let product = require('./products.model');

const cart = db.define('cart', {

    cart_id: {
        type: Sequelize.STRING,
        allowNull: false
    },
    product_id: {
        type: Sequelize.BIGINT.UNSIGNED,
        allowNull: false
    },
    quantity: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    price: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    shipping_cost: {
        type: Sequelize.FLOAT,
        allowNull: true,
    },
    sub_total: {
        type: Sequelize.FLOAT,
        allowNull: false,
    },
    total: {
        type: Sequelize.FLOAT,
        allowNull: false,
    }
}, {
    underscored: true,
    paranoid: true
});


cart.belongsTo(product, {
    foreignKey: 'product_id',
    sourceKey: 'product_id'
});
module.exports = cart;