let Sequelize = require('sequelize');


const product = db.define('product', {
    product_id: {
        type: Sequelize.BIGINT.UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.TEXT,
        allowNull: true
    },
    image: {
        type: Sequelize.STRING,
        allowNull: true
    },
    sku: {
        type: Sequelize.STRING,
        allowNull: false
    },
    price: {
        type: Sequelize.FLOAT,
        allowNull: false
    },    
    category_id: {
        type: Sequelize.BIGINT.UNSIGNED,
        allowNull: false
    },
    isFeatured: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
        defaultValue: '0'
    },
    tag: {
        type: Sequelize.ENUM('new','sale',''),
        allowNull: true,
        defaultValue:'new'
    },
    type: {
        type: Sequelize.STRING,
        allowNull: true
    },
    color: {
        type: Sequelize.STRING,
        allowNull: true
    },
    model_name: {
        type: Sequelize.STRING,
        allowNull: true
    },
    series: {
        type: Sequelize.STRING,
        allowNull: true
    },
    brand: {
        type: Sequelize.STRING,
        allowNull: true
    },
    stock: {
        type: Sequelize.ENUM('available','out-of-stock'),
        allowNull: true,
        defaultValue:'available'
    },
    discount_price: {
        type: Sequelize.FLOAT,
        allowNull: true
    }
}, {
    underscored: true,
    paranoid: true
});

category.hasMany(product, {
    foreignKey: 'category_id',
    sourceKey: 'category_id'
});

module.exports = product;
