let Sequelize = require('sequelize');

const category = db.define('category', {

    category_id: {
        type: Sequelize.BIGINT.UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false,
    }
}, {
    underscored: true,
    paranoid: true
});


module.exports = category;
