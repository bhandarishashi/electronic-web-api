let Sequelize = require('sequelize');
let product = require('./products.model');

const image = db.define('image', {
    image_id: {
        type: Sequelize.BIGINT.UNSIGNED,
        primaryKey: true,
        autoIncrement: true
    },
    product_id: {
        type: Sequelize.BIGINT.UNSIGNED,
        allowNull: false
    },
    full_image: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    thumbnail_image: {
        type: Sequelize.STRING,
        allowNull: true,
    }
}, {
    underscored: true,
    paranoid: true
});

product.hasMany(image, {
    foreignKey: 'product_id',
    sourceKey: 'product_id'
});

module.exports = image;
