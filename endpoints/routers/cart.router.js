var express = require('express');
var router = express.Router();

var cartController = require('../controllers/cart.controller');

//get the cart item
router.route('/:cart_id')
    .get(cartController.getCart);

//create the cart
router.route('/')
    .post(cartController.createCart);



//remove the cart item
router.route('/remove/:cart_id/:product_id')
    .delete(cartController.removeCartItem);



module.exports = router;