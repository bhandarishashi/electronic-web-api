var express = require('express');
var router = express.Router();

var categoryController = require('../controllers/category.controller');

//get all category
router.route('/')
    .get(categoryController.getCategories);


//get products by category
router.route('/:category_key')
    .get(categoryController.getProductsByCategory);

//get products by category
router.route('/recommend/:category_key')
    .get(categoryController.getRecommendProductsByCategory);

module.exports = router;