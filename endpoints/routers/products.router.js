var express = require('express');
var router = express.Router();

var productController = require('../controllers/products.controller');

//get all products
router.route('/')
    .get(productController.getProducts);


//get detail of a  product
router.route('/:id')
    .get(productController.getProductDetail);


module.exports = router;