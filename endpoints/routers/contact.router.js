var express = require('express');
var router = express.Router();

var contactController = require('../controllers/contact.controller');

//submit the inquiry
router.route('/')
    .post(contactController.postInquiry);



module.exports = router;