module.exports = function(app){

    var categoryRouter = require('../endpoints/routers/category.router');
    var productRouter = require('../endpoints/routers/products.router');
    var contactRouter = require('../endpoints/routers/contact.router');
    var cartRouter = require('../endpoints/routers/cart.router');

    app.use('/category',categoryRouter);
    app.use('/product',productRouter);
    app.use('/contact',contactRouter);
    app.use('/cart',cartRouter);


}
