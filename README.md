
# Sassy Electronics 

## Technical Documentation

### Project Setup

#### Install Node and Npm

+ For downloading node visit [Download Node](https://markdownlivepreview.com/) .
+ Clone the project from the [gitlab](https://gitlab.com/bhandarishashi/sassy-electronics-api.git).
+ Go to the project. For example: `cd projects/SassyElectronicsApi`
+You will be in the master branch.
+ Run `npm install` , it will install all the prerequisite packages.
+ Run `nodemon`
+ The project will be hosted in [Sassy Electronics](http://www.manaram.com:3000).

#### Regarding Database (MySQL)
+ Has used the online database [Remote Mysql](https://remotemysql.com/phpmyadmin/) for easy access.
+ Used Sequelize for query processing as it is a promise-based Node.js ORM for MySQL.
+ You donot have to install it separately while running `running npm install` sequelize too will be installed.
+ Credential for database :

     + `Username: rNKyMIJ4RY`
     + `Password: dZrnAfSLDh`
     

##### Remarks:
+ During development:

    
|               | Versions      |
| ------------- |:-------------:|
| Node          | 12.18.4       |
| Npm           | 6.14.6        |
| Sequelize     | ^6.6.2        |

+  Versions can be seen on the package.json file.
+ Had tested in node version 10.14.2, it has worked smoothly.


##### Further help
To get any help or had any queries do contact me via mail [Shashee Bhandari](https://www.gmail.com) 

`email`: shasheebhandari@gmail.com


## API Documentation

### Category [/category]

#### List All Categories [GET]

+ Request (application/json)
    + Headers
    
            Location:  /category

+ Response 200 (application/json)

    + Body

        {

            "success": true,
            "valid": true,
            "message": "Categories found.",
            "categories": [
                {
                "category_id": 1,
                "name": "Mobiles",
                "createdAt": "2021-06-14T00:00:00.000Z",
                "updatedAt": "2021-06-14T00:00:00.000Z",
                "deletedAt": null
                },
                {
                "category_id": 2,
                "name": "Laptops",
                "createdAt": "2021-06-14T00:00:00.000Z",
                "updatedAt": "2021-06-14T00:00:00.000Z",
                "deletedAt": null
                }
            ]
        }


#### List All Products of Category [GET]

+ Request (application/json)
    + Headers
    
            Location:  ?category_id=category_id

+ Response 200 (application/json)

    + Body

        {

            "success": true,
            "valid": true,
            "message": "Products found.",
            "products": {
            "count": 4,
            "rows": [

            {
                "product_id": 3,
                "name": "Canon DSLR",
                "description": "EOS Utility Webcam Beta Software (Mac and Windows) to turn your Canon camera into a high-quality webcam, or do the same using a clean HDMI output.",
                "image": "http://localhost:3000/uploads/camera1.jpg",
                "sku": "opi8uy6d",
                "price": 95000,
                "category_id": 4,
                "isFeatured": true,
                "tag": "new",
                "createdAt": "2021-06-14T00:00:00.000Z",
                "updatedAt": "2021-06-14T00:00:00.000Z",
                "deletedAt": null
            },
            ]
            },
            "category": {
                "category_id": 4,
                "name": "Cameras",
                "createdAt": "2021-06-14T00:00:00.000Z",
                "updatedAt": "2021-06-14T00:00:00.000Z",
                "deletedAt": null
            },
            "total_page": 2

         }


#### List Recommnended Products of Category [GET]

+ Request (application/json)
    + Headers
    
            Location:  /recommend/category_id?product_id=pid

+ Response 200 (application/json)

    + Body

       {

            "success": true,
            "valid": true,
            "message": "Products found.",
            "products": [
                {
                "product_id": 2,
                "name": "Iphone",
                "description": "The face detection, high dynamic range and optical image stabilization. It is capable of capturing 4K video at 24, 30 or 60 frames per second, or 1080p video at 30, 60, 120 or 240 frames per second.",
                "image": "http://localhost:3000/uploads/mobile1.jpg",
                "sku": "312e3668f3ae ",
                "price": 100000,
                "category_id": 1,
                "isFeatured": true,
                "tag": "new",
                "createdAt": "2021-06-14T00:00:00.000Z",
                "updatedAt": "2021-06-14T00:00:00.000Z",
                "deletedAt": null

                }
            ],
            "category": {

            "category_id": 1,
            "name": "Mobiles",
            "createdAt": "2021-06-14T00:00:00.000Z",
            "updatedAt": "2021-06-14T00:00:00.000Z",
            "deletedAt": null

            }

        }


### Product [/product]

#### List All Products [GET]

+ Request (application/json)
    + Headers
    
            Location:  /product

+ Response 200 (application/json)

    + Body
        {

            "success": true,
            "valid": true,
            "message": "Products found.",
            "products": {
            "count": 15,
            "rows": [
                {
                    "product_id": 1,
                    "name": "Samsung",
                    "description": "Samsung Galaxy J7 is a mid range Android smartphone produced by Samsung Electronics in 2015.",
                    "image": "http://localhost:3000/uploads/mobileJ7.jpg",
                    "sku": "b2237158",
                    "price": 25000,
                    "category_id": 1,
                    "isFeatured": false,
                    "tag": "sale",
                    "createdAt": "2021-06-14T00:00:00.000Z",
                    "updatedAt": "2021-06-14T00:00:00.000Z",
                    "deletedAt": null
            
                }
               ]
             },
             "featured_products": {
                "count": 7,
                "rows": [
                {
                    "product_id": 2,
                    "name": "Iphone",
                    "description": "The iPhone X (Roman numeral \"X\" pronounced \"ten\")[10][11] is a smartphone designed, developed, marketed, produced, and sold by Apple Inc.",
                    "image": "http://localhost:3000/uploads/mobile1.jpg",
                    "sku": "312e3668f3ae ",
                    "price": 100000,
                    "category_id": 1,
                    "isFeatured": true,
                    "tag": "new",
                    "createdAt": "2021-06-14T00:00:00.000Z",
                    "updatedAt": "2021-06-14T00:00:00.000Z",
                    "deletedAt": null

                }

                ]
             }

        }


#### Detail of specific Product [GET]

+ Request (application/json)
    + Headers
    
            Location: /product_id

+ Response 200 (application/json)

    + Body
        {

            "success": true,
            "valid": true,
            "message": "Product found.",
            "product": {

                    "product_id": 1,
                    "name": "Samsung",
                    "description": "Samsung Galaxy J7 is a mid range An",
                    "image": "http://localhost:3000/uploads/mobileJ7.jpg",
                    "sku": "b2237158",
                    "price": 25000,
                    "category_id": 1,
                    "isFeatured": false,
                    "tag": "sale",
                    "createdAt": "2021-06-14T00:00:00.000Z",
                    "updatedAt": "2021-06-14T00:00:00.000Z",
                    "deletedAt": null,
                    "images": [

                    {

                    "image_id": 1,
                    "product_id": 1,
                    "full_image": "http://localhost:3000/uploads/mobileJ7.jpg",
                    "thumbnail_image": "http://localhost:3000/uploads/thumbmobileJ7.jpg",
                    "createdAt": "2021-06-15T00:00:00.000Z",
                    "updatedAt": "2021-06-15T00:00:00.000Z",
                    "deletedAt": null
                   
                    },
                    {

                    "image_id": 2,
                    "product_id": 1,
                    "full_image": "http://localhost:3000/uploads/mobile4.jpg",
                    "thumbnail_image": "http://localhost:3000/uploads/thumbmobile4\r\n.jpg",
                    "createdAt": "2021-06-15T00:00:00.000Z",
                    "updatedAt": "2021-06-15T00:00:00.000Z",
                    "deletedAt": null

                    }

                    ]
                }

        }
       
    
### Contact Us [/contact] 

#### Contact Us [POST]

+ Request (application/json)
    + Headers
    
            Location:  /contact

                
    + Body
    
            {

            	"full_name": "John Doe",
                "email": "john@doe.com",
                "subject": "About Product Inquiry",
                "message": "Integer vehicula sollicitudin magna, vel faucibus odio ultricies gravida. Proin quis diam augue. Etiam vitae tellus cursus, lobortis lacus at, suscipit lectus. Nullam id est urna. Donec nec auctor sem, posuere dignissim diam. Quisque convallis diam ultricies fringilla molestie. Integer condimentum mi at rutrum sollicitudin."

            }

+ Response 200 (application/json)

    + Body

       {

            "success": true,
            "valid": true,
            "message": "Message has been submitted successfully.",

        }


    
### Cart [/cart] 

#### Create Cart [POST]

+ Request (application/json)
    + Headers
    
            Location:  /cart

                
    + Body
    
            {

            	"product_id": "1",
                "quantity": "1",
                "price": "50000",
                "sub_total": "50000",
                "total":  "50000"

            }

+ Response 200 (application/json)

    + Body

       {
            "success": true,
            "valid": true,
            "message": "Item has been added successfully.",

        }
        
#### Get Cart [GET]

+ Request (application/json)
    + Headers
    
            Location:  /cart/cart_id

+ Response 200 (application/json)

    + Body

       {

           "success": true,
            "valid": true,
            "message": "Cart found.",
            "cart": [
                        {
                        "id": 1,
                        "cart_id": "61920756",
                        "product_id": 2,
                        "quantity": 2,
                        "price": "100000",
                        "shipping_cost": null,
                        "sub_total": 200000,
                        "total": 200000,
                        "createdAt": "2021-06-15T02:08:16.000Z",
                        "updatedAt": "2021-06-15T02:08:16.000Z",
                        "deletedAt": null,
                        "product": {
                        "product_id": 2,
                        "name": "Iphone",
                        "description": "The iPhone X (Roman numeral \"X\" pronounced \"ten\")[10][11] is a smartphone designed, developed, marketed, produced, and sold by Apple Inc.",
                        "image": "https://manaramprojects.com:3000/uploads/mobile1.jpg",
                        "sku": "312e3668f3ae ",
                        "price": 100000,
         
                    }
                }
            ]


          }
          
                  
#### Remove Item From Cart [DELETE]

+ Request (application/json)
    + Headers
    
            Location:  /cart/cart_id/product_id

+ Response 200 (application/json)

    + Body

       {

           "success": true,
            "valid": true,
            "message": "Item has been removed.",

 
          }


### Thank you.