

/**
 *
 * @param {*} res
 * @param {*} err
 *
 * List the error
 */

module.exports.errorHandler = async (res,err ) => {
    var status = err.status || 500;
    res.status(status).json({success:false, valid: false, message:err.message, err:err});
}